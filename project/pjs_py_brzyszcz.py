#!/usr/bin/python
# Dawid Brzyszcz - Pracownia Jezykow Skryptowych, gr. 1

import os
import sys
import getopt
import time
from urllib2 import urlopen, URLError, Request
from urlparse import urlparse
from threading import Thread
from Queue import Queue
from contextlib import contextmanager


url_filename = None
num_threads = 1
download_path = './'


def print_help():
    print "\n***Program zaliczeniowy z jezyka Python***"
    print "\nProgram sluzy do pobierania plikow z podanych adresow url przez http."
    print "\nPrzykladowe wywolanie:\t./pjs_py_brzyszcz.py http://some.address/file.extension"
    print "\nZaprezentowane powyzej wywolanie spowoduje sciagniecie pliku z podanego adresu do " \
          "lokalizacji z ktorej zostal wywolany skrypt."
    print "Mozna w ten sposob podac kilka adresow."
    print "\nDostepne opcje:"
    print "\t-h, --help\twyswietla pomoc"
    print "\t-f, --file\topcjonalnie - plik z ktorego maja byc wczytane adresy. Kazdy adres " \
          "musi sie znajdowac w nowej linijce"
    print "\t-t, --threads\topcjolanie - liczba watkow ktora ma pobierac pliki. Jesli mamy dluga liste adresow, warto " \
          "zwiekszyc liczbe watkow w celu przyspieszenia pobierania."
    print "\t-d, --dir\topcjonalnie - sciezka katalogu w ktorym maja byc zapisane pliki"
    print "\nSkrypt domyslnie pobiera pliki 1 watkiem."
    print "\nPrzykladowe wywolanie uruchamiajace pobieranie czteroma watkami adresow z pliku " \
          "i zapisujace pliki do sprecyzowanego folderu:"
    print "\t./pjs_py_brzyszcz.py -f fileWithLinks.txt -t 4 -d /path/to/downloaded/"
    print ""
    print "Uwaga: program nie wspiera pobierania rekurencyjnego."
    print ""


def normalize_path(path):
    if not path.startswith('/'):
        path = os.path.join(os.getcwd(), path)
        path = os.path.abspath(path)
    return path if os.path.isdir(path) else None


def resolve_args(optlist, remainder):
    for arg, value in optlist:
        if arg in ('-h', '--help'):
            print_help()
            sys.exit(0)
        elif arg in ('-f', '--file'):
            global url_filename
            url_filename = value
        elif arg in ('-t', '--threads'):
            global num_threads
            try:
                num_threads = int(value)
            except ValueError:
                print "Niepoprawna liczba watkow: %s" % value
                sys.exit(1)
        elif arg in ('-d', '--dir'):
            path = normalize_path(value)
            if path:
                global download_path
                download_path = path
            else:
                print "Niepoprawna sciezka: %s" % value
                sys.exit(1)
        else:
            print_help()
            sys.exit(0)


def is_url_valid(url):
    parsed = urlparse(url)
    if not bool(parsed.netloc):
        print >> sys.stderr, "Pomijam nieprawidlowy adres url: %s" % url
        return False
    return True


def read_urls(filename, delimeter='\n', lower_limit=None, upper_limit=None):
    with open(filename) as f:
        content = f.read()
    urls = content.split(delimeter)
    if lower_limit:
        urls = urls[lower_limit:]
    if upper_limit:
        urls = urls[:upper_limit]
    return urls


def download(url):
    filename = url.split('/')[-1]

    if filename == '':
        print >> sys.stderr, "Podany url nie prowadzi do pliku (url: %s)" % url
        return

    path = os.path.join(download_path, filename)
    try:
        request = Request(url, headers={'User-Agent': 'CustomAgent/1.0'})
        response = urlopen(request, timeout=2)
        if response.getcode() == 200:
            with open(path, 'w') as f:
                f.write(response.read())
        else:
            raise URLError
    except Exception:
        print >> sys.stderr, "Nie udalo sie pobrac pliku z %s" % url


def perform_download(thread_no, queue):
    while True:
        url = queue.get()
        print "[THREAD-%d] Pobiera z: %s" % (thread_no, url)
        download(url)
        queue.task_done()


def spawn_threads(queue):
    for i in range(num_threads):
        worker = Thread(target=perform_download, args=(i, queue))
        worker.daemon = True
        worker.start()


def fill_queue(queue, urls):
    for url in urls:
        if is_url_valid(url):
            queue.put(url)


@contextmanager
def measure_time():
    start_time = time.time()
    yield
    elapsed = time.time() - start_time
    print "*** Skonczono w czasie: %fs ***" % elapsed


#
# Program workflow
#
try:
    optlist, remainder = getopt.getopt(sys.argv[1:], 'hf:t:d:', ['help', 'file=','threads=', 'dir='])
except getopt.GetoptError:
    print "Niepoprawne argumenty"
    print_help()
    sys.exit(1)

resolve_args(optlist, remainder)

url_queue = Queue()

if url_filename:
    urls = read_urls(url_filename)
else:
    urls = remainder

with measure_time():
    fill_queue(url_queue, urls)
    spawn_threads(url_queue)
    url_queue.join()
