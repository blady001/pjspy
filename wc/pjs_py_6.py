#!/usr/bin/python
# Dawid Brzyszcz - Pracownia Jezykow Skryptowych gr.1

import sys
import os
import getopt
from py_module import is_int, is_real

exclude_hashlines = False
count_int = False
count_real = False

opts, args = getopt.getopt(sys.argv[1:], 'die')


def clear_arg(args, arg):
    try:
        args.remove(arg)
    except ValueError:
        pass


for opt, value in opts:
    if opt == '-d':
        count_real = True
    elif opt == '-i':
        count_int = True
    elif opt == '-e':
        exclude_hashlines = True

clear_arg(args, '-d')
clear_arg(args, '-i')
clear_arg(args, '-e')


global_line_count = 0
global_word_count = 0
global_character_count = 0
global_int_count = 0
global_real_count = 0
global_byte_count = 0
file_count = 0

for filename in args:
    line_count = 0
    word_count = 0
    character_count = 0
    int_count = 0
    real_count = 0
    byte_count = 0

    try:
        with open(filename) as f:
            lines = f.readlines()
    except IOError:
        print >> sys.stderr, 'Nie udalo sie otworzyc pliku: %s' % filename
    else:
        line_count = len(lines)
        global_line_count += len(lines)

        for line in lines:
            words = [x.strip() for x in line.split(' ') if x.strip() is not '']
            word_count += len(words)
            global_word_count += len(words)

            for word in words:
                character_count += len(word)
                global_character_count += character_count

                if is_int(word):
                    int_count += 1
                    global_int_count += 1
                if is_real(word):
                    real_count += 1
                    global_real_count += 1

        byte_count = os.path.getsize(filename)
        global_byte_count += byte_count

        print "%s - statystyki:" % filename
        if count_int:
            print "Liczby calkowite: %d" % int_count
        if count_real:
            print "Liczby rzeczywiste: %d" % real_count
        if len(opts) == 0:
            print "Linie: %d" % line_count
            print "Slowa: %d" % word_count
            print "Znaki: %d" % character_count
            print "Bajty: %d" % byte_count

        file_count += 1

if file_count > 1:
    print "Podsumowanie dla %d plikow" % file_count

    if count_int:
        print "Liczby calkowite: %d" % global_int_count
    if count_real:
        print "Liczby rzeczywiste: %d" % global_real_count
    if len(opts) == 0:
        print "Linie: %d" % global_line_count
        print "Slowa: %d" % global_word_count
        print "Znaki: %d" % global_character_count
        print "Bajty: %d" % global_byte_count
