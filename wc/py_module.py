import re


def is_int(value):
    try:
        i = int(value)
        return True
    except ValueError:
        return False


def is_real(value):
    return True if re.match(r'^[-+]?[0-9]*\.?[0-9]+([eE^][-+]?[0-9]+)?$', value) else False
